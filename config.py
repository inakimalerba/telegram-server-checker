from helpers.host import Host
from modules import up_checker, test

CHAT_ID = 123456
TOKEN = 'YOUR TOKEN'

HOSTS_LIST = [
    Host('HOSTNAME', 'IP'),
]

MODULES = [
    up_checker.UpChecker,
    test.Test
]
