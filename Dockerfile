FROM debian:unstable

RUN apt-get update && apt-get install -y \
    python3 python3-pip

RUN pip3 install python-telegram-bot --upgrade
