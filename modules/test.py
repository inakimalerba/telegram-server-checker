from telegram.ext import Updater, CommandHandler, MessageHandler, Filters


class Test:
    name = 'Test'

    def __init__(self, hosts, chat_id, token):
        self.hosts = hosts
        self.chat_id = chat_id
        self.updater = Updater(token=token, use_context=True)

        test_handler = CommandHandler('test', self._test)
        self.updater.dispatcher.add_handler(test_handler)

        echo_handler = MessageHandler(Filters.text, self._cmd)
        self.updater.dispatcher.add_handler(echo_handler)

    def _check_user(self, update):
        if update.effective_chat.id == self.chat_id:
            return True
        return False

    @staticmethod
    def _test(update, context):
        context.bot.send_message(chat_id=update.effective_chat.id, text="I'm a bot, please talk to me!")

    @staticmethod
    def _cmd(update, context):
        context.bot.send_message(chat_id=update.effective_chat.id, text=update.message.text)

    def run(self):
        self.updater.start_polling()
