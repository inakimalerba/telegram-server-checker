import time

from helpers.notifier import Notifier

PERIOD_S = 60


class UpChecker:
    name = 'UpChecker'

    def __init__(self, hosts, chat_id, token):
        self.hosts = hosts
        self.notifier = Notifier(chat_id, token)

    @staticmethod
    def _get_status_message(host):
        icon = "💩" if host.status == 'OFF' else "👌"
        return f"{icon} {host.hostname} is {host.status}"

    def run(self):
        while True:
            for host in self.hosts:
                if host.status_changed():
                    self.notifier.send_message(self._get_status_message(host))

            time.sleep(PERIOD_S)
