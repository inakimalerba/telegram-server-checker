import _thread

import config
from helpers.notifier import Notifier

def main():
    notifier = Notifier(config.CHAT_ID, config.TOKEN)
    notifier.send_message("🤖 Starting")

    for module in config.MODULES:
        instance = module(config.HOSTS_LIST, config.CHAT_ID, config.TOKEN)
        notifier.send_message(f'🙋 Module loaded: {instance.name}')

        _thread.start_new_thread(instance.run, ())

    while True:
        pass


if __name__ == '__main__':
    main()
