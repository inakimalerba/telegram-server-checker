import telegram

class Notifier:
    def __init__(self, chat_id, token):
        self.chat_id = chat_id
        self.api = telegram.Bot(token=token)

    def send_message(self, text):
        return self.api.send_message(chat_id=self.chat_id, text=text)
