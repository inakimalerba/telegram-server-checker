import os

ON = 0
OFF = 1

class Host:
    def __init__(self, hostname, ip):
        self.hostname = hostname
        self.ip = ip
        self.last_status = ON

    def status_changed(self):
        current_status = self._get_status()

        if self.last_status != current_status:
            self.last_status = current_status
            return True

        return False

    def _get_status(self):
        status = OFF

        for i in range(3):
            status &= os.system("ping -q -c 1 " + self.ip + " > /dev/null 2>&1") != 0
            if status == ON: break

        return status

    @property
    def status(self):
        return 'ON' if self.last_status == ON else 'OFF'
